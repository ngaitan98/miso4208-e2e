# Cypress
1. Abrir Cypress
2. Añadir manualmente el directorio
3. Darle click al archivo simple-spec.js
# Puppeteer
1. Ir a la carpeta Puppeteer `cd Puppeteer`
2. Instalar dependencias `npm install`
3. Ejecutar el script `node example.js`