describe('Los estudiantes', function () {
    it('Visits los estudiantes and fails at login', function () {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.contains('Ingresar').click()
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail@example.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("1234")
        cy.get('.cajaLogIn').contains('Ingresar').click()
        cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
    })
    it('Visits los estudiantes and logins correctly', function () {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.contains('Ingresar').click()
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("n.gaitan@uniandes.edu.co")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("Habbo123")
        cy.get('.cajaLogIn').contains('Ingresar').click()
    })
    it('Visits los estudiantes and registers with an existing email', function () {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.contains('Ingresar').click()
        cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("John")
        cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Doe")
        cy.get('.cajaSignUp').find('input[name="correo"]').click().type("n.gaitan@uniandes.edu.co")
        cy.get('.cajaSignUp').find('select[name="idPrograma"]').select("Ingeniería de Sistemas y Computación")
        cy.get('.cajaSignUp').find('input[name="password"]').click().type("Habbo123")
        cy.get('.cajaSignUp').find('input[name="acepta"]').click()
        cy.get('.cajaSignUp').contains('Registrarse').click()
        cy.contains('Ya existe un usuario');
    })
    it('Visits los estudiantes and fails to find a teacher', function () {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.get('.Select').find('input').type('Hello World!',{force:true})
        cy.contains("No se encontraron profesores ni materias")
    })
    it('Visits los estudiantes and finds a teacher correctly', function () {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.get('.Select').find('input').type('Mario Linares Vasquez',{force:true})
        cy.get('.Select-menu-outer').contains('Mario Linares Vasquez -')
    })
    it('Visits los estudiantes and visits a teacher correctly', function () {
        cy.visit('https://losestudiantes.co')
        cy.contains('Cerrar').click()
        cy.get('.Select').find('input').type('Mario Linares Vasquez',{force:true})
        cy.get('.Select-menu-outer').contains('Mario Linares Vasquez -').click()
        cy.get('.columnLeft').contains('Mario Linares Vasquez');
    })
    it('Visits los estudiantes and filters a teacher correctly', function (){
        cy.visit('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/mario-linares-vasquez');
        cy.get('input[name="id:MISO4208"]').click()
    })
})