const puppeteer = require('puppeteer');
(async () => {
  const browser = await puppeteer.launch({headless:false, slowMo: 100});
  const page = await browser.newPage();
  await page.goto('https://angular-6-registration-login-example.stackblitz.io/register');
  //Esto es para que la página termine de cargar
  await page.waitFor(5000)
  await page.type('form input[formcontrolname="firstName"]', 'Nicolás')
  await page.type('form input[formcontrolname="lastName"]', 'Gaitán')
  await page.type('form input[formcontrolname="username"]', 'ngaitan98')
  await page.type('form input[formcontrolname="password"]', '123456')
  await page.screenshot({path: './screenshots/registerInfo.png'});
  await page.click('form button')
  await page.waitFor(2000)
  await page.screenshot({path: './screenshots/registerSuccess.png'});
  console.log("[Success]: Registro");
  await page.type('form input[formcontrolname="username"]', 'ngaitan98')
  await page.type('form input[formcontrolname="password"]', '123456')
  await page.screenshot({path: './screenshots/loginInfo.png'});
  await page.click('form button')
  await page.waitFor(2000)
  await page.screenshot({path: './screenshots/loginSuccess.png'});
  console.log("[Success]: login");
  await page.goto('https://angular-6-registration-login-example.stackblitz.io/register');
  //Esto es para que la página termine de cargar
  await page.waitFor(5000)
  await page.type('form input[formcontrolname="firstName"]', 'Nicolás')
  await page.type('form input[formcontrolname="lastName"]', 'Gaitán')
  await page.type('form input[formcontrolname="username"]', 'ngaitan98')
  await page.type('form input[formcontrolname="password"]', '123456')
  await page.click('form button')
  await page.waitFor(2000)
  await page.screenshot({path: './screenshots/registerSuccess.png'})
  console.log("[Error]: Registro");
  await browser.close();
})();